#pragma once


namespace pcs {
	enum CollisionChecking {
		DISCRETE_COLLISION_CHECKING,
		CONTINUOUS_COLLISION_CHECKING
	};
}