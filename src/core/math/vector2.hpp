#pragma once

#include <math.h>


namespace pcs::math {
	/**
	 * @brief A mathematical 2d vector.
	 */
	struct Vec2
	{
	public:
		// X coordinate
		float x;
		// Y coordinate
		float y;

	public:
		/**
		 * @brief Create a vector of length zero.
		 * @return The equivalent a vector with components 0.0f
		 */
		inline static Vec2 Zero()
		{
			return Vec2 { 0.0f, 0.0f };
		}

	public:
		/**
		 * @brief Constructs a vector. The coordinates are not
		 * intialized.
		 * @return The vector created
		*/
		inline Vec2() = default;

		/**
		 * @brief Constructs a vector.
		 * 
		 * @param xCoordinate The x coordinate
		 * @param yCoordinate The y coordinate
		 * @return The vector created
		*/
		inline Vec2(float xCoordinate, float yCoordinate)
			: x(xCoordinate), y(yCoordinate)
		{
		}

		/**
		 * @brief Calculates the length of the vector. May be more
		 * expensive then SqrdLength because of the use of the square
		 * root function.
		 * @return The length
		 */
		inline float Length() const
		{
			return sqrt(x * x + y * y);
		}

		/**
		 * @brief Calculates the squared length of the vector.
		 * @return The squared length
		 */
		inline float SqrdLength() const
		{
			return x * x + y * y;
		}

		/**
		 * @brief Calculates the dot product of the vector with an
		 * other.
		 * @return The dot product
		 */
		inline float Dot(Vec2 other) const
		{
			return x * other.x + y * other.y;
		}

		inline Vec2 Unit() const
		{
			Vec2 unit = { 0.0f, 0.0f };
			float sqrdLength = x * x + y * y;

			if (sqrdLength != 0.0f) {
				float length = sqrt(sqrdLength);
				unit.x = x / length;
				unit.y = y / length;
			}

			return unit;
		}


		inline Vec2 operator+(void) const
		{
			return Vec2 { x , y };
		}

		inline Vec2 operator-(void) const
		{
			return Vec2 { -x, -y };
		}

		inline Vec2 operator+(Vec2 other) const
		{
			return Vec2 { x + other.x, y + other.y };
		}

		inline Vec2 operator-(Vec2 other) const
		{
			return Vec2 { x - other.x, y - other.y };
		}

		inline Vec2 operator*(float scalar) const
		{
			return Vec2 { x * scalar, y * scalar };
		}

		inline Vec2 operator/(float scalar) const
		{
			return Vec2 { x / scalar, y / scalar };
		}

		inline void operator+=(Vec2 other)
		{
			x += other.x;
			y += other.y;
		}

		inline void operator-=(Vec2 other)
		{
			x -= other.x;
			y -= other.y;
		}

		inline void operator*=(float scalar)
		{
			x *= scalar;
			y *= scalar;
		}

		inline void operator/=(float scalar)
		{
			x /= scalar;
			y /= scalar;
		}
	};
}