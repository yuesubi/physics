#pragma once

#include <memory>
#include <vector>

#include "../body/rigid_body.hpp"
#include "../body/rigid_body_blueprint.hpp"
#include "../math/vector2.hpp"


namespace pcs {
	// A 2d physics world
	class World
	{
	public:
		class RigidBody_Ptr
		{
		public:
			RigidBody *operator->(void);
			RigidBody& operator*(void);
		
		private:
			World& containing_world;
			std::size_t rigid_body_index;
		
		private:
			friend World;

			RigidBody_Ptr(World& containing_world,
				std::size_t rigid_body_index);
		};
	
	public:
		World();
		World(math::Vec2 gravity, float timeStep);
		~World();

		void SetGravity(math::Vec2 newGravity);
		math::Vec2 GetGravity() const;

		void SetStep(float newTimeStep);
		float GetStep() const;

		// Add a rigid body so it gets stimulated
		RigidBody_Ptr CreateRigidBody(const RigidBody_Blueprint& blueprint);

		// Calculate one step
		void Step();

	private:
		// The gravity in the world
		math::Vec2 m_Gravity;
		// All the bodies of the world
		std::vector<RigidBody> m_rigid_bodies;
		// The time step
		float m_TimeStep;
	};
}