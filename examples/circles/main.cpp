#include "raylib-cpp/Window.hpp"
#include "physics.hpp"

#include <iostream>


using namespace pcs;
using namespace pcs::math;


#define WIN_WIDTH  640
#define WIN_HEIGHT 480

#define TIME_STAMP 0.01f

#define UNIT ((float) WIN_WIDTH / 21.0f)


int main()
{
	raylib::Window win(WIN_WIDTH, WIN_HEIGHT, "Physics: circles example");

	// WORLD
	World world;
	world.SetGravity({0, -9.81});
	world.SetStep(TIME_STAMP);

	/* <CIRCLE> */
	RigidBody_Blueprint circ_rb_bp;
	circ_rb_bp.position = Vec2(0.0f, 0.0f);
	circ_rb_bp.rotation = 0.0f;
	circ_rb_bp.volumic_mass = 1.0f;

	World::RigidBody_Ptr circle_rb = world.CreateRigidBody(circ_rb_bp);

	CircleCollider circColl(0.5f);
	circle_rb->AttachCollider(&circColl);
	/* </CIRCLE> */

	/* <AABB> */
	RigidBody_Blueprint rect_rb_bp;
	rect_rb_bp.position = Vec2(0.0f, 0.0f);
	rect_rb_bp.rotation = 0.0f;
	rect_rb_bp.volumic_mass = 1.0f;

	World::RigidBody_Ptr aabb_rb = world.CreateRigidBody(rect_rb_bp);

	AABBCollider aabb_collider(1.0f, 1.0);
	aabb_rb->AttachCollider(&aabb_collider);
	/* </AABB> */


	// win.SetTargetFPS(100);
	float time_buffer = 0.0f;

	while (!win.ShouldClose()) {
		time_buffer += win.GetFrameTime();
		if (time_buffer > 0.2f) {
			time_buffer = 0.2f;
		}

		while (time_buffer > TIME_STAMP) {
			world.Step();
			time_buffer -= TIME_STAMP;
		}

		BeginDrawing();
			ClearBackground(BLACK);

			DrawRectangleLines(
				(int) ((aabb_rb->position.x - 0.5f) * UNIT + WIN_WIDTH/2),
				(int) ((-aabb_rb->position.y - 0.5f) * UNIT + WIN_HEIGHT/2),
				(int) UNIT, (int) UNIT,
				BLUE
			);
			DrawCircleLines(
				(int) (circle_rb->position.x * UNIT + WIN_WIDTH/2),
				(int) (-circle_rb->position.y * UNIT + WIN_HEIGHT/2),
				(int) (0.5f * UNIT),
				RED
			);
			DrawFPS(10, 10);
		EndDrawing();
	}
}